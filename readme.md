# Usage
Program accepts filepath from arguments.

```java -jar <jar_file> [csv_file]```

If csv_file is not provided, "test.csv" will be picked

# Input format
Csv format: id,firstName,lastName,salary,managerId

First line can be csv header

# Functionality
Process given file and prints:
- which managers earn less than they should, and by how much
- which managers earn more than they should, and by how much
- which employees have a reporting line which is too long, and by how much