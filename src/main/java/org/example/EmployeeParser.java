package org.example;

import java.util.Map;

public interface EmployeeParser {
    Map<Integer, TreeNode<Employee>> getEmployeesTreeNodeMap();
}
