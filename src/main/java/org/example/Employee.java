package org.example;

public class Employee {
    private final String firstName;
    private final String lastName;
    private final int id;
    private final Integer managerId;
    private final double salary;

    public Employee(String firstName, String lastName, int id, Integer managerId, double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.managerId = managerId;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getId() {
        return id;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
