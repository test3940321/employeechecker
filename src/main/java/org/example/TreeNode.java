package org.example;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {
    private T content;
    private TreeNode<T> parent;
    private List<TreeNode<T>> children;
    private int height;

    public TreeNode(T content) {
        this.content = content;
        children = new ArrayList<>();
    }

    public TreeNode(T content, TreeNode<T> parent) {
        this(content);
        this.parent = parent;
    }

    public T getContent() {
        return content;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }


    public void addChild(TreeNode<T> child) {
        children.add(child);
    }

    public int getHeight() {
        return height;
    }

    public void updateHeights(int height) {
        this.height = height;
        for (TreeNode<T> child : children) {
            child.updateHeights(height + 1);
        }
    }
}
