package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.DoubleStream;

public class EmployeeChecker {
    private static final double SALARY_NO_MORE_TIMES_THEN = 1.5;
    private static final double SALARY_NO_LESS_TIMES_THEN = 1.2;
    private static final int MAX_MANAGERS = 4;

    private TreeNode<Employee> root;
    private final Map<Integer, TreeNode<Employee>> employees;

    public EmployeeChecker(EmployeeParser parser) {
        this.employees = parser.getEmployeesTreeNodeMap();
        organizeTreeNodes();
        checkValidTree();
    }

    /**
     * @return List of employees which have too long reporting line.
     */
    public List<Employee> getOverReportingEmployees() {
        List<Employee> overReporting = new ArrayList<>();

        for (TreeNode<Employee> employeeTreeNode : employees.values()) {
            if (employeeTreeNode.getHeight() > MAX_MANAGERS) {
                overReporting.add(employeeTreeNode.getContent());
            }
        }
        return overReporting;
    }

    /**
     * @return List of managers which have too small salary comparing to their direct subordinates.
     */
    public List<Employee> getUnderPayedManagers() {
        List<Employee> underPayed = new ArrayList<>();

        for (TreeNode<Employee> employeeTreeNode : employees.values()) {
            Employee employee = employeeTreeNode.getContent();
            double avg = getAverageSalary(employeeTreeNode);

            if (avg != 0 && employee.getSalary() < avg * SALARY_NO_LESS_TIMES_THEN) {
                underPayed.add(employee);
            }
        }
        return underPayed;
    }

    /**
     * @return List of managers which have too big salary comparing to their direct subordinates.
     */
    public List<Employee> getOverPayedManagers() {
        List<Employee> overPayed = new ArrayList<>();

        for (TreeNode<Employee> employeeTreeNode : employees.values()) {
            Employee employee = employeeTreeNode.getContent();
            double avg = getAverageSalary(employeeTreeNode);

            // avg == 0 means it is not a manager
            if (avg != 0 && employee.getSalary() > avg * SALARY_NO_MORE_TIMES_THEN) {
                overPayed.add(employee);
            }
        }
        return overPayed;
    }

    private double getAverageSalary(TreeNode<Employee> employee) {
        return employee.getChildren().stream()
                .flatMapToDouble(
                        employeeTreeNode ->
                                DoubleStream.of(employeeTreeNode.getContent().getSalary())
                ).average().orElse(0);
    }

    /**
     * Makes structured tree from separate nodes through connecting parents with children
     *
     * @throws IllegalStateException when given employees have no / multiple CEOs or no manager node was found.
     */
    private void organizeTreeNodes() {
        for (TreeNode<Employee> employeeTreeNode : employees.values()) {
            Employee employee = employeeTreeNode.getContent();
            Integer managerId = employee.getManagerId();

            if (managerId == null) {
                if (root == null) {
                    root = employeeTreeNode;
                    continue;
                } else {
                    throw new IllegalStateException("Multiple CEOs are not allowed!");
                }
            }

            TreeNode<Employee> parent = employees.get(managerId);
            if (parent == null) {
                throw new IllegalStateException("No manager with ID " + managerId);
            }

            employeeTreeNode.setParent(parent);
            parent.addChild(employeeTreeNode);
        }

        if (root == null) {
            throw new IllegalStateException("No CEO found!");
        }
        root.updateHeights(0);
    }

    /**
     * Checks if tree has no cycles (ie all nodes connected to the tree)
     *
     * @throws IllegalStateException if cycle was found
     */
    private void checkValidTree() {
        for (TreeNode<Employee> employeeTreeNode : employees.values()) {
            // If any node has 0 height then this node isn't reachable from root (CEO)
            // and if it has parent then it is a cycle and not a CEO
            if (employeeTreeNode.getHeight() == 0 && employeeTreeNode.getParent() != null) {
                throw new IllegalStateException("No cyclic relationships between employees allowed!");
            }
        }
    }
}
