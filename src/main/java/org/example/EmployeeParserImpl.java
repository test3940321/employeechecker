package org.example;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation of EmployeeParser interface,
 * uses InputStream as a dependency for acquiring Employee data.
 *
 * Parses input csv with employees to Map<TreeNode<Employee>>>
 */
public class EmployeeParserImpl implements EmployeeParser {

    private static final int ID_FIELD = 0;
    private static final int FIRST_NAME_FIELD = 1;
    private static final int LAST_NAME_FIELD = 2;
    private static final int SALARY_FIELD = 3;
    private static final int MANAGER_ID_FIELD = 4;
    private static final int TOTAL_FIELDS_COUNT = 5;

    private final InputStream inputStream;

    public EmployeeParserImpl(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * Parses csv coming from InputStream.
     * Csv format: id,firstName,lastName,salary,managerId
     * First line can be header
     *
     * @return map with id as key and TreeNode from parsed Employee as value
     */
    public Map<Integer, TreeNode<Employee>> getEmployeesTreeNodeMap() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        List<String> lines = br.lines().collect(Collectors.toList());
        boolean isFirstLine = true;

        for (String line : lines) {
            String[] fields = line.split(",", TOTAL_FIELDS_COUNT);

            if (isFirstLine && !fields[ID_FIELD].matches("\\d+")) {
                isFirstLine = false; // first line can be a csv header
                continue;
            }
            isFirstLine = false;

            Employee employee = createEmployeeFromFields(fields);
            TreeNode<Employee> employeeTreeNode = new TreeNode<>(employee);

            employees.put(employee.getId(), employeeTreeNode);
        }
        return employees;
    }

    private Employee createEmployeeFromFields(String[] fields) {
        double salary = Double.parseDouble(fields[SALARY_FIELD]);

        if (salary <= 0) {
            throw new IllegalStateException("Salary must be greater then 0!");
        }

        return new Employee(
                fields[FIRST_NAME_FIELD],
                fields[LAST_NAME_FIELD],
                Integer.parseInt(fields[ID_FIELD]),
                fields[MANAGER_ID_FIELD].isEmpty() ? null : Integer.valueOf(fields[MANAGER_ID_FIELD]),
                Double.parseDouble(fields[SALARY_FIELD]));
    }
}
