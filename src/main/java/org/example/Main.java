package org.example;

import java.io.*;
import java.nio.file.Files;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File file;

        if (args.length > 0) {
            file = new File(args[0]);
        } else {
            file = new File("test.csv");
        }

        EmployeeChecker checker;

        try (InputStream inputStream = Files.newInputStream(file.toPath())) {
            checker = new EmployeeChecker(new EmployeeParserImpl(inputStream));
        } catch (IOException e) {
            throw new RuntimeException("Unable to read a file", e);
        }

        printEmployeesList(checker.getOverPayedManagers(), "Over payed managers");
        printEmployeesList(checker.getUnderPayedManagers(), "Under payed managers");
        printEmployeesList(checker.getOverReportingEmployees(), "Over reporting employees");
    }

    private static void printEmployeesList(List<Employee> employees, String listName) {
        System.out.println(listName);
        for (Employee e : employees) {
            System.out.println(e);
        }
        System.out.println();
    }
}