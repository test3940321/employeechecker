package org.example;

import java.util.Map;

/**
 * Mock implementation of EmployeeParser for testing
 */
public class MockEmployeeParser implements EmployeeParser {
    private Map<Integer, TreeNode<Employee>> employees;

    public void setEmployeesTreeNodeMap(Map<Integer, TreeNode<Employee>> employees) {
        this.employees = employees;
    }

    public Map<Integer, TreeNode<Employee>> getEmployeesTreeNodeMap() {
        return employees;
    }
}
