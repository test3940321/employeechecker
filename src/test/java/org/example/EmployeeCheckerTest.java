package org.example;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class EmployeeCheckerTest {

    @Test
    public void testValidUnderPayedManager() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        Employee[] expected = {
                new Employee("1", "1", 1, null, 10000)
        };
        employees.put(1, new TreeNode<>(expected[0]));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, 1, 100000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        EmployeeChecker checker = new EmployeeChecker(parser);
        assertArrayEquals(expected, checker.getUnderPayedManagers().toArray());
    }

    @Test
    public void testValidOverPayedManager() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        Employee[] expected = {
                new Employee("1", "1", 1, null, 10000)
        };
        employees.put(1, new TreeNode<>(expected[0]));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, 1, 1000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        EmployeeChecker checker = new EmployeeChecker(parser);
        assertArrayEquals(expected, checker.getOverPayedManagers().toArray());
    }

    @Test
    public void testValidOverReportingEmployee() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        Employee[] expected = {
                new Employee("6", "6", 6, 5, 10000)
        };
        employees.put(1, new TreeNode<>(new Employee("1", "1", 1, null, 10000)));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, 1, 10000)));
        employees.put(3, new TreeNode<>(new Employee("3", "3", 3, 2, 10000)));
        employees.put(4, new TreeNode<>(new Employee("4", "4", 4, 3, 10000)));
        employees.put(5, new TreeNode<>(new Employee("5", "5", 5, 4, 10000)));
        employees.put(6, new TreeNode<>(expected[0]));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        EmployeeChecker checker = new EmployeeChecker(parser);
        assertArrayEquals(expected, checker.getOverReportingEmployees().toArray());
    }

    @Test
    public void testWhenMultipleCEOsThenException() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        employees.put(1, new TreeNode<>(new Employee("1", "1", 1, null, 10000)));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, null, 10000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        IllegalStateException exception = assertThrows(IllegalStateException.class,
                () -> new EmployeeChecker(parser));
        assertEquals("Multiple CEOs are not allowed!", exception.getMessage());
    }

    @Test
    public void testWhenNoManagerWithIdFoundThenException() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        employees.put(1, new TreeNode<>(new Employee("1", "1", 1, null, 10000)));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, 3, 10000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        IllegalStateException exception = assertThrows(IllegalStateException.class,
                () -> new EmployeeChecker(parser));
        assertEquals("No manager with ID 3", exception.getMessage());
    }

    @Test
    public void testWhenCycleFoundThenException() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        employees.put(1, new TreeNode<>(new Employee("1", "1", 1, null, 10000)));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, 3, 10000)));
        employees.put(3, new TreeNode<>(new Employee("3", "3", 3, 2, 10000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        IllegalStateException exception = assertThrows(IllegalStateException.class,
                () -> new EmployeeChecker(parser));
        assertEquals("No cyclic relationships between employees allowed!", exception.getMessage());
    }

    @Test
    public void testWhenCycleOnHimselfFoundThenException() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        employees.put(1, new TreeNode<>(new Employee("1", "1", 1, null, 10000)));
        employees.put(2, new TreeNode<>(new Employee("2", "2", 2, 2, 10000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        IllegalStateException exception = assertThrows(IllegalStateException.class,
                () -> new EmployeeChecker(parser));
        assertEquals("No cyclic relationships between employees allowed!", exception.getMessage());
    }

    @Test
    public void testWhenNoCEOFoundThenException() {
        Map<Integer, TreeNode<Employee>> employees = new HashMap<>();
        employees.put(1, new TreeNode<>(new Employee("1", "1", 1, 2, 10000)));
        employees.put(2, new TreeNode<>(new Employee("1", "1", 2, 1, 10000)));

        MockEmployeeParser parser = new MockEmployeeParser();
        parser.setEmployeesTreeNodeMap(employees);

        IllegalStateException exception = assertThrows(IllegalStateException.class,
                () -> new EmployeeChecker(parser));
        assertEquals("No CEO found!", exception.getMessage());
    }
}
