package org.example;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import static org.junit.Assert.*;

public class EmployeeParserTest {

    @Test
    public void testValidCEO() {
        String initialString = "1,f,l,10000,";
        InputStream inputStream = new ByteArrayInputStream(initialString.getBytes());
        EmployeeParser parser = new EmployeeParserImpl(inputStream);
        Map<Integer, TreeNode<Employee>> employees = parser.getEmployeesTreeNodeMap();

        assertEquals(1, employees.size());

        TreeNode<Employee> employeeNode = employees.get(1);
        assertNotNull(employeeNode);

        Employee employee = employeeNode.getContent();
        assertNotNull(employee);

        assertEquals(1, employee.getId());
        assertEquals("f", employee.getFirstName());
        assertEquals("l", employee.getLastName());
        assertEquals(10000., employee.getSalary(), 0.001);
        assertNull(employee.getManagerId());
    }

    @Test
    public void testValidEmployee() {
        String initialString = "1,f,l,10000,2\n";
        InputStream inputStream = new ByteArrayInputStream(initialString.getBytes());
        EmployeeParser parser = new EmployeeParserImpl(inputStream);
        Map<Integer, TreeNode<Employee>> employees = parser.getEmployeesTreeNodeMap();

        assertEquals(1, employees.size());

        TreeNode<Employee> employeeNode = employees.get(1);
        assertNotNull(employeeNode);

        Employee employee = employeeNode.getContent();
        assertNotNull(employee);

        assertEquals(1, employee.getId());
        assertEquals("f", employee.getFirstName());
        assertEquals("l", employee.getLastName());
        assertEquals(10000., employee.getSalary(), 0.001);
        assertEquals(Integer.valueOf(2), employee.getManagerId());
    }

    @Test
    public void testInvalidId() {
        String initialString = "header,f,l,s,mid\ntest,1,1,1,1";
        InputStream inputStream = new ByteArrayInputStream(initialString.getBytes());
        EmployeeParser parser = new EmployeeParserImpl(inputStream);

        assertThrows(NumberFormatException.class, parser::getEmployeesTreeNodeMap);
    }

    @Test
    public void testInvalidSalary() {
        String initialString = "1,f,l,-1,";
        InputStream inputStream = new ByteArrayInputStream(initialString.getBytes());
        EmployeeParser parser = new EmployeeParserImpl(inputStream);

        IllegalStateException exception = assertThrows(IllegalStateException.class, parser::getEmployeesTreeNodeMap);
        assertEquals("Salary must be greater then 0!", exception.getMessage());
    }

    @Test
    public void testInvalidManagerId() {
        String initialString = "1,f,l,10000,test";
        InputStream inputStream = new ByteArrayInputStream(initialString.getBytes());
        EmployeeParser parser = new EmployeeParserImpl(inputStream);

        assertThrows(NumberFormatException.class, parser::getEmployeesTreeNodeMap);
    }
}
